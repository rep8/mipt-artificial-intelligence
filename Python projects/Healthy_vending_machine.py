# -*- coding: utf-8 -*-

''' This is a reagular vending machine offers sport and healthy beverages and snaks
I might be installed at gyms and at parks for everyine using ''' 

def inputCheck(question, options):

    user_input = input(question)

    while (user_input not in options):
        print("That is not a valid input.")
        user_input = input(question)

    return user_input


class VendingMachine:

    # here is list of items
    item_choices = {'protein cookie': 50, 'zero sugar cola': 60,
                    'isotonic': 100, 'no sugar candy': 10}
    # customer credit value
    user_credit = 0
    # customer item cost
    user_item_cost = 0
    # customer balance due
    user_money_due = 0
    # customer item choice
    user_item_choice = ""

    # constructor
    def __init__(self):
        pass

    # Desribing process
    def intro(self):
        # list option of items user can purchase and price (from json file)
        welcome = 'Hello, athlete! \n'
        print(welcome)
        print('Here are what I have for you:')
        for key in self.item_choices.keys():
            print(key + ' {:0.2f}RUB'.format(self.item_choices[key]))

    # Customer chooses item
    def itemChoosing(self):

        # Ask customer to choose which item they want
        pickItemQuestion = '\n What would you eat today? '
        itemOptions = self.item_choices.keys()

        self.user_item_choice = inputCheck(pickItemQuestion, itemOptions)
        self.user_item_cost = self.item_choices.get(self.user_item_choice)

        print('You have selected ' + self.user_item_choice + '.')
        print('That will cost {:0.2f}RUB\n'.format(self.user_item_cost))

        if (self.user_credit >= self.user_item_cost):
            self.checkEnoughMoney()
        else:
            self.payment()

        return self.user_item_cost

    # Payment

    def payment(self):

        # Payment options
        paymentOptions = {'1RUB': 1, '10RUB': 10, '50RUB': 50, '100RUB': 100}

        # List of payment
        pay_options_prompt = "Please pay by inserting the following '1RUB' = 1 RUB, '10RUB' = 10 RUB, '50RUB' = 50 RUB, '100RUB' = 100 RUB \n"

        # Asking the customer to input money
        inserted_money = inputCheck(pay_options_prompt, paymentOptions.keys())

        # Update customer's credit 
        self.user_credit += paymentOptions.get(inserted_money)
        self.user_money_due = self.user_item_cost - self.user_credit

        # Asking customer to add money
        if (self.user_money_due > 0):
            print('You have {:0.2f}RUB'.format(
                self.user_money_due) +  'left to pay.\n')
            self.askInsertMore()
        else:
            self.askInsertMore()

    # Asks customer if they want to insert more money
    def askInsertMore(self):

        insertMoreQuestion = "Would you like to add money? '+' = yes, '-' = no \n"
        insertMoreOptions = ['+', '-']

        insertMoreYN = inputCheck(insertMoreQuestion, insertMoreOptions)

        if (insertMoreYN == '+'):
            self.payment()
        else:  # insertMoreYN == '-'
            self.checkEnoughMoney()

    # Check if entered money is enough to buy chosen item
    def checkEnoughMoney(self):

        # Customer has inserted enough money
        if (self.user_money_due <= 0):

            # Write-off the cost of item from credit
            self.user_credit -= self.user_item_cost

            # Giving the choosen item
            print('Hey! Grab your ' + self.user_item_choice)
            print('You have {:0.2f}RUB'.format(self.user_credit) + ' left')

            # Asking if customers wants something else
            if (self.askBuyMore()):
                self.itemChoosing()
            else:
                self.returnChange()

        else:
            print("Thank you! It's enough")
            self.askInsertMore()

    def askBuyMore(self):

        # Asking if they want to buy more
        buyMoreQuestion = "Do you want somethong else? \n"
        buyMoreResponses = ['+', '-']
        buyMoreYN = inputCheck(buyMoreQuestion, buyMoreResponses)

        if (buyMoreYN):
            return True
        else:
            return False

    # Returning the change
    def returnChange(self):

        # Saying customer thank
        thankyou_prompt = 'Thank you! Be healthy and do your best'

        print('Your change is {:0.2}RUB'.format(self.user_credit))
        print(thankyou_prompt)


# Main
machine1 = VendingMachine()
machine1.intro()
machine1.itemChoosing()
